#pragma once
#include "../Application.hpp"

class MutableNumber
{
public:
    // Constructeurs
    MutableNumber(double value, double proba, double sigma, bool inf_b = false, double inf_d =0, bool sup_b = false, double sup_d =0);
    MutableNumber(j::Value const& config);
    MutableNumber()=default;

    // getter/setter
    double get() const;
    void set(const double& value);

    // Methodes
    void mutate();
    double check_borne(double value) const;

    // Methodes de génération de nombres mutables
    static MutableNumber probability(double initialValue, double mutationProbability, double sigma);
    static MutableNumber probability(j::Value const& config);
    static MutableNumber positive(double initialValue, double mutationProbability, double sigma, bool hasMax = false, double max = 0);
    static MutableNumber positive(j::Value const& config, bool hasMax, double max);

private:

    double proba_mutation_;
    bool borne_inf;
    bool borne_sup;
    double borne_inf_;
    double borne_sup_;
    double ecart_type_;
    double valeur_;
};

