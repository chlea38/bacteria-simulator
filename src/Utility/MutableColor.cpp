#include "MutableColor.hpp"

// Constructeur______________________________________________________________________________________________
MutableColor::MutableColor(j::Value const& config)
    : RGBA_tab {config["r"],config["g"] ,
                config["b"], config["a"] }
{}


// Getter____________________________________________________________________________________________________
sf::Color MutableColor::get() const
{
  // Retourne la couleur en combinant toutes les composantes sous forme SFML
    return { static_cast<sf::Uint8>(RGBA_tab[0].get() * 255),
             static_cast<sf::Uint8>(RGBA_tab[1].get() * 255),
             static_cast<sf::Uint8>(RGBA_tab[2].get() * 255),
             static_cast<sf::Uint8>(RGBA_tab[3].get() * 255) };

}

// Méthodes__________________________________________________________________________________________________
void MutableColor::mutate()
{
    // Pour chaque composante de la couleur dans le tableau
    for (auto& color : RGBA_tab)
    {
        // On fait muter le MutableNumber représentaant la composante
        color.mutate();
    }
}
