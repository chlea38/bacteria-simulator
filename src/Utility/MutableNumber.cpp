#include "MutableNumber.hpp"
#include "../Random/Random.hpp"

// Constructeurs_____________________________________________________________________________________
//Initialise avec des valeurs passées en paramètre
MutableNumber::MutableNumber(double value, double proba, double sigma,
                             bool inf_b, double inf_d, bool sup_b, double sup_d )
    :
      proba_mutation_(proba), borne_inf(inf_b), borne_sup(sup_b),
      borne_inf_(inf_d), borne_sup_(sup_d), ecart_type_(sigma), valeur_(value)
{
     // on vérifie que value est dans les bornes
    valeur_=check_borne(valeur_);
}

// Initialise avec un fichier de configuration
MutableNumber::MutableNumber(j::Value const& config)
    :
      proba_mutation_(config["rate"].toDouble()),
      borne_inf(config["clamp min"].toBool()),
      borne_sup(config["clamp max"].toBool()),
      borne_inf_(config["min"].toDouble()),
      borne_sup_(config["max"].toDouble()),
      ecart_type_(config["sigma"].toDouble()),
      valeur_(config["initial"].toDouble())

{
    valeur_=check_borne(valeur_);
}

// getter/ setter _________________________________________________________________________________________
double MutableNumber::get() const
{
    return valeur_;
}

void MutableNumber::set(const double& value)
{
    // On vérifie que value est dans les bornes (si elles existent)
    valeur_ = check_borne(value);
}

// Methodes___________________________________________________________________________________________________
void MutableNumber::mutate()
{
   bool mutation;
   // on crée un bool mutation qui prend la valeur 1 avec une probabilité proba_mutation
   // et la valeur 0 sinon, en utilisant la fonction bernoulli
   mutation = bernoulli(proba_mutation_);

   // Si mutation vaut 1 (soit true), on tire un nb aléatoire selon
   // la distribution normale et on l'ajoute à la valeur du MutableNumber
   if (mutation) {
       double nb_aleatoire(0.);
       nb_aleatoire = normal(0, ecart_type_*ecart_type_);
       valeur_ = check_borne(valeur_+nb_aleatoire); // On vérifie que la valeur ne dépasse pas les bornes
   }
}

double MutableNumber::check_borne(double value) const
{
    double valeur(value);

    // s'il existe des bornes, on vérifie que value les respecte
    if (borne_inf) valeur = std::max(valeur, borne_inf_);
    if (borne_sup) valeur = std::min(valeur, borne_sup_);

    // Retourne value ou une des deux bornes si value ne les respectait pas
    return valeur;
}


// Methodes de génération de nombres mutables_________________________________________________________________________
MutableNumber MutableNumber::probability(double initialValue,
                                         double mutationProbability, double sigma)
{
    // Création d'un MutableNumber de type probabilité: borné entre 0 et 1
    MutableNumber nombre(initialValue, mutationProbability, sigma, true, 0.0, true, 1.0);
    return nombre;
}

MutableNumber MutableNumber::probability(j::Value const& config)
{
    // Récupération des valeurs du fichier de configuration
    double initialValue(config["initial"].toDouble());
    double mutationProbability(config["rate"].toDouble());
    double sigma(config["sigma"].toDouble());

    // Création d'un MutableNumber de type probabilité: borné entre 0 et 1
    MutableNumber nombre(initialValue, mutationProbability, sigma, true, 0.0, true, 1.0);
    return nombre;
}

MutableNumber MutableNumber::positive(double initialValue, double mutationProbability,
                                      double sigma, bool hasMax, double max)
{
    // Création d'un MutableNumber de type positif : borne minimale à 0
    MutableNumber nombre(initialValue, mutationProbability, sigma, true, 0.0, hasMax, max);
    return nombre;
}

MutableNumber MutableNumber::positive(j::Value const& config, bool hasMax, double max)
{
    // Récupération des valeurs du fichier de configuration
    double initialValue(config["initial"].toDouble());
    double mutationProbability(config["rate"].toDouble());
    double sigma(config["sigma"].toDouble());

    // Création d'un MutableNumber de type positif : borne minimale à 0
    MutableNumber nombre(initialValue, mutationProbability, sigma, true, 0.0, hasMax, max);
    return nombre;
}
