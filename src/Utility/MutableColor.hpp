#pragma once
#include"MutableNumber.hpp"
#include <array>
#include <SFML/Graphics.hpp>

class MutableColor
{
public:
    // Constructeur
    MutableColor(j::Value const& config);

    // Getter -> retourne la couleur
    sf::Color get() const;

    // Méthodes
    void mutate();

private:
    MutableNumber RGBA_tab[4];
};

