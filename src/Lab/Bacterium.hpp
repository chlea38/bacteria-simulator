#pragma once
#include "CircularBody.hpp"
#include "../Interface/Drawable.hpp"
#include "../Interface/Updatable.hpp"
#include "../Utility/Types.hpp"
#include "../Utility/Vec2d.hpp"
#include<SFML/Graphics.hpp>
#include "../Utility/MutableNumber.hpp"
#include <string>
#include "../Utility/MutableColor.hpp"
#include "../Utility/DiffEqSolver.hpp"
#include <array>

class Nutriment;
class NutrimentA;
class NutrimentB;
class Virus;
class Bacterium :public CircularBody, public Updatable, public Drawable
{
public:
    //Constructeur
    Bacterium(Quantity energy, Vec2d center,Vec2d direction, double radius, MutableColor color);

    //Destructeur
    virtual ~Bacterium();

    //Getter
    virtual MutableNumber getProperty(const std::string& key) const; //Retrouver une valeur mutable associée à une clé donnée

    //Méthodes de mise à jour à chaque boucle
    virtual void drawOn(sf::RenderTarget& target) const override;    // Dessin de la bactérie
    virtual void update(sf::Time dt) override;                       // Mise à jour de la bactérie

    //Méthode d'évolution des Bacterium
    virtual void move(sf::Time dt)=0;                                // Déplacement de la bactérie
    virtual Bacterium* clone()const=0;                               // retourne un clone de la bactérie
    void divide();                                                   // division de la bactérie
    void mutate();                                                   // mutation des paramètres mutables de la bactérie
    bool isDead() const;                                             // Teste si une bactérie est morte (energie<=0)
    void consumeEnergy (Quantity const& qt);                         // Baisse d'energie
    void addProperty(const std::string& key, MutableNumber nb);      //Ajouter à l'ensemble des paramètres mutables numériques de la bactérie une valeur numérique mutable donnée

    // Méthodes spécifiques à la nutrition des bactéries
    void eat (Nutriment& nutriment);                              // Augmente l'énergie à la suite de la nutrition
    virtual Quantity eatableQuantity(NutrimentA& nutriment) = 0;  // Fait appel à la méthode eatenBy de nutrimentA sur le bon type de bactérie
    virtual Quantity eatableQuantity(NutrimentB& nutriment) = 0;  // Fait appel à la méthode eatenBy de nutrimentB sur le bon type de bactérie

    //Méthodes pour faciliter accès aux données (Getter)
    virtual j::Value const& getConfig() const =0;  // Accès aux données de chaque type de bactéries dans App.JSON
    Quantity getMinEnergy() const;                 // Retourne l'énergie minimale nécessaire à la division
    Quantity getMaxEatableQuantity() const;        // Retourne la quantité maximale de nutriment qu'une bactérie peut consommer
    sf::Time getTime ()const;                      // Retourne le temps d'attente entre deux consommations de nutriments
    virtual Quantity getConsEnergy ()const;        // Retourne l'énergie dépensée à chaque pas de déplacement

    //Méthodes d'extension
    void updateAttackers(sf::Time dt);             // Met à jour le virus qui attaque la bactérie s'il y en a un
    Case& getCase();                               //Renvoie la case sur laquelle se trouve une bactérie (en utilisant sa position)
    virtual bool isVulnerable() const=0;           //Méthode polymorphique qui permet de déterminer si un type de bactérie est vulnérable aux KillerBacterium
    void isAttacked();                             // Méthode polymorphique appelée à chaque update si la bactérie est la cible d'un virus
    void setAttackers(Virus*);                     // Met à jour l'attaquant de la bactérie
    bool isTarget() const;                         // Renvoie si la bactérie est la cible d'un virus ou non
    void updateCase();


protected:
    Quantity energy_;                                           // niveau d'énergie de la bactérie
    Vec2d direction_;                                           // vecteur de la direction de la bactérie
    double angleDirectionRad;                                   // angle que forme la direction avec l'horizontal
    MutableColor color_;                                        // couleur mutable de la bactérie
    sf::Time tBasculement;                                      // compteur du temps entre chaque changement soudain de direction (basculement)
    std::map<std::string, MutableNumber> mutableParameters;     // ensemble des paramètres mutables
    Virus* attackers;                                           // pointeur sur le virus qui l'attaque
    Vec2d case_;                                                // coordonnées (x,y) de la case dans laquelle se trouve la bactérie

private:
    bool abstinence_;      // la bactérie peut manger ou non
    sf::Time compteur;     // compteur de temps
};

