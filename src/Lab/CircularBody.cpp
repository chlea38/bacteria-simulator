#include "CircularBody.hpp"
#include<iostream>


// Constructeur___________________________________________
CircularBody::CircularBody(Vec2d center, double radius)
    : center_(center)
    , radius_(radius)
{}

// Constructeur de copie___________________________________
CircularBody::CircularBody(const CircularBody& other)
    : center_ (other.center_)
    , radius_ (other.radius_)
{}

//destructeur__________________________________________________________________________________________________
CircularBody::~CircularBody(){}

// Getters/setters_____________________________________________________________________________________________
Vec2d CircularBody::getPosition() const
{
    return center_;
}

void CircularBody::setPosition(const Vec2d& center)
{
    center_ = center;
}

double CircularBody::getRadius() const
{
    return radius_;
}

void CircularBody::setRadius(const double& radius)
{
    radius_ = radius;
}

// Operateur interne___________________________________________________________________________________________
CircularBody& CircularBody:: operator=(CircularBody const& other)
{
   center_ = other.center_;
   radius_ = other.radius_;
   return (*this);
}


// Methodes____________________________________________________________________________________________________
void CircularBody::move(const Vec2d& center)
{
    // change le centre du CircularBody
    center_ += center;
}

bool CircularBody::contains(const CircularBody& other) const
{
    // Verifie si other est contenu dans le CircularBody
    return (radius_>=other.radius_ and
     distance(center_, other.center_)<= (radius_ - other.radius_));
}

bool CircularBody::isColliding(const CircularBody& other) const
{
    // Verifie la collision entre deux CircularBody
    return (distance(center_, other.center_) <= (radius_ + other.radius_));
}

bool CircularBody::contains(const Vec2d& point) const
{
    // Verifie si le point est contenu dans le CircularBody
   return (distance(point, center_)<= radius_);
}

// Operateurs externes_________________________________________________________________________________________

bool operator>(const CircularBody& body1, const CircularBody& body2)
{
    //Verifie si body 2 est contenu dans body 1
    return (body1.contains(body2));
}

bool operator&(const CircularBody& body1, const CircularBody& body2)
{
    // Verifie si body 1 et body 2 sont en collision
    return (body1.isColliding(body2));
}

bool operator>(const CircularBody& body1, const Vec2d& point)
{
    // Verifie si point est contenu dans body 1
    return (body1.contains(point));
}

std::ostream& operator<<(std::ostream& out, const CircularBody& body)
{
    out<< "CircularBody : position = " << body.getPosition()
       << ", radius = " << body.getRadius() << std::endl;
   return out;
}
