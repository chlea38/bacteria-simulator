#include "NutrimentA.hpp"
#include "Application.hpp"
#include "Bacterium.hpp"
#include "SimpleBacterium.hpp"
#include "TwitchingBacterium.hpp"
#include "SwarmBacterium.hpp"
#include "KillerBacterium.hpp"


//Constructeur_________________________________________________________________________________________________
NutrimentA::NutrimentA(Quantity quantity, Vec2d center) : Nutriment(quantity, center)
{}

//Destructeur--------------------------------------------------------------------------------------------------
NutrimentA::~NutrimentA(){}

// Méthode spécifiques à la nutrition des bactéries____________________________________________________________
Quantity NutrimentA:: eatenBy(Bacterium& bact)
{
    return bact.eatableQuantity(*this); // Oriente sur la méthode eatableQuantity du bon type de bactérie
}
Quantity NutrimentA::eatenBy(SimpleBacterium& bact)
{
    return takeQuantity(bact.getMaxEatableQuantity());
}

Quantity NutrimentA::eatenBy(TwitchingBacterium &bact)
{
    return takeQuantity(bact.getMaxEatableQuantity());
}
Quantity NutrimentA::eatenBy(SwarmBacterium& bact)
{
    return takeQuantity(bact.getMaxEatableQuantity());
}

Quantity NutrimentA::eatenBy(KillerBacterium& bact)
{
    double factor2 (getConfig()["poison factor"].toDouble());
    Quantity qte(std::min(quantity_, bact.getMaxEatableQuantity()));
    return -takeQuantity(qte*factor2);
}

//Méthode pour faciliter accès aux données_____________________________________________________________________
j::Value const& NutrimentA:: getConfig() const
{
    return (getAppConfig()["nutriments"]["A"]);
}
