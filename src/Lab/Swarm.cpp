#include "Swarm.hpp"
#include "SwarmBacterium.hpp"
#include "../Utility/Utility.hpp"

// Constructeur__________________________________________________________________________________
Swarm::Swarm(std::string id)
    : id_(id),
      leader_(nullptr)
{}

// Destructeur______________________________________________________________________________________
Swarm::~Swarm()
{
    // Desalocation de la mémoire des pointeurs
    for (auto& bact: bacterium_)
    {
        // pas de delete car la propriété appartient à PetriDish
        bact=nullptr;
    }
    bacterium_.clear();
    leader_=nullptr;
}

// Getter_______________________________________________________________________________________

std::string Swarm::getId() const
{
    return id_;
}

j::Value const& Swarm::getConfig() const
{
    return getAppConfig()["swarms"];
}

Vec2d Swarm::getPosLeader() const
{
    // On retourne la position de la bacterie leader
    Vec2d pos;
    if (leader_ == nullptr) {
        pos = bacterium_[0]->getPosition();
    } else {
        pos = leader_->getPosition();
    }
    return pos;
}

SwarmBacterium* Swarm::getLeader() const
{
    // Retourne la bactérie leader
    return leader_;
}


MutableColor Swarm::getStartColor() const
{
    MutableColor color(getConfig()[id_]["color"]);
    return color;
}

// Methodes____________________________________________________________________________________
void Swarm::update(sf::Time dt)
{
    double score = 0;

    for (auto& bact : bacterium_) // recherche du meilleur score parmis toute les bactéries
    {
        double score_bact = getAppEnv().getPositionScore(bact->getPosition());
        if (score_bact>score)
        {
            score = score_bact;
            leader_ = bact; // Mise à jour du leader si meilleur score obtenu
        }
    }
}

void Swarm::drawOn(sf::RenderTarget& target) const
{
    for (auto& swarmbact: bacterium_)
    {
        if (swarmbact==leader_ and isDebugOn())
        {
            // Dessiner un cercle rouge d'epaisseur 2
            auto border = buildAnnulus(swarmbact->getPosition(), swarmbact->getRadius() + 10, sf::Color::Red, 2);
            target.draw(border);
        }
    }
}

bool Swarm::addBacterium(SwarmBacterium* bact)
{
    // Ajout de bact dans l'ensemble de bacteries
    if (getAppEnv().contains(*bact)) //Test pour ne pas que la bactérie dépasse de la boite de pétri
    {
      bacterium_.push_back(bact);
      return true;
    }
    return false;
}

void Swarm::deleteBacterium(SwarmBacterium* bact)
{
    // Suppression de Bact dans l'ensemble de bactéries du swarm
    bacterium_.erase(std::remove(bacterium_.begin(), bacterium_.end(), bact), bacterium_.end());
}





