#pragma once
#include "Nutriment.hpp"

class NutrimentB : public Nutriment
{
public:
    //Constructeur
    NutrimentB (Quantity quantity, Vec2d center);

    //Destructeur
    virtual ~NutrimentB();

    // Méthode spécifiques à la nutrition des bactéries
    virtual Quantity eatenBy(Bacterium& bact) override;                   // Oriente sur la méthode eatableQuantity de la bonne sous-classe de bactérie
    virtual Quantity eatenBy(SimpleBacterium& bact) override;             // Nutrition spécifique au type de nutriment ET de bactérie
    virtual Quantity eatenBy(TwitchingBacterium& bact) override;
    virtual Quantity eatenBy(SwarmBacterium& bact) override;
    virtual Quantity eatenBy(KillerBacterium& bact) override;


    //Méthode pour faciliter accès aux données
    virtual j::Value const& getConfig() const override;  // permet l'accès aux valeurs du fichier de configuration de Nutriment B


};
