#include "Virus.hpp"
#include "../Random/Random.hpp"
#include "Application.hpp"
#include "Bacterium.hpp"
#include "KillerBacterium.hpp"
#include<SFML/Graphics.hpp>
#include "../Utility/Utility.hpp"

Virus::Virus(Vec2d center, KillerBacterium* carrier): CircularBody(center, getConfig()["radius"].toDouble()),
    energy(uniform(getConfig()["energy"]["min"].toDouble(), getConfig()["energy"]["max"].toDouble())),
    target(nullptr), carrier(carrier), compteur(sf::Time::Zero)
{}

Virus::~Virus()
{
    // Désallocation des pointeurs : pas de delete car pas la propriété
    target=nullptr;
    carrier=nullptr;
}

j::Value const& Virus::getConfig() const
{
    return (getAppConfig()["virus"]);
}

void Virus::drawOn(sf::RenderTarget& target) const
{
    //Léger décalage par rapport à la bactérie pour pouvoir mieux observer
    Vec2d position (0, +15);

    //Dessin d'un virus
    auto const& texture = getAppTexture(this->getConfig()["texture"].toString());     // récupération image du virus
    auto virusSprite = buildSprite(getPosition()+position, 6, texture);
    virusSprite.setScale(2 * getRadius() / texture.getSize().x, 2 * getRadius() / texture.getSize().y);
    target.draw(virusSprite);

    //Affichage en mode debug : affiche energie du virus
    if(isDebugOn())
    {
    //Valeur du rayon (arrondie à l'unité)
    int quantite(floor(energy));

    //Dessin du texte
    Vec2d position (0, +25); // Léger décalage pour que le texte ne soit pas sur le virus
    auto const text = buildText(std::to_string(quantite), getPosition()+position ,getAppFont(),15,sf::Color::Blue);
    target.draw(text);
    }
}

void Virus::update(sf::Time dt)
{
    compteur+=dt;
    // déplacement du virus
    move(dt);

    //récupération de la valeur de temps minimale entre 2 attaques
    auto const& time=getConfig()["attack delay"].toDouble();
    sf::seconds(time);

    // Attaque de la cible si le virus en a une
    if (target!=nullptr and compteur>=sf::seconds(time))
    {
        target->isAttacked();
        consumeEnergy(getConfig()["energy"]["attack consumption"].toDouble()); // Consommation d'énergie léie à l'attaque
        compteur=sf::Time::Zero;
    }
}

void Virus::move(sf::Time dt)
{
    // Si le virus est porté par une KillerBacterium : il la suit(porté par elle)
    if (carrier!=nullptr)
    {
        setPosition(carrier->getPosition());
    }

    // Si le virus a infecté une bacterium : il la suit (porté par elle)
    if (target!=nullptr)
    {
        setPosition(target->getPosition());
    }

    // Pas de consommation d'énergie liée au déplacement car ce sont les bactéries qui portent le virus en elles
}


 void Virus::setTarget(Bacterium* bact)
 {
     // Se "détache" de sa killerBacterium porteuse
     carrier=nullptr;

     // Prend la bactérie pour cible
     target= bact;
 }

 bool Virus::isDead() const
 {
     // Mort si énergie à 0
     return (energy<=0);
 }

 void Virus:: consumeEnergy (Quantity const& qt)
 {
     // Diminution de l'énergie
     energy=std::max(energy-qt,0.0);
 }
