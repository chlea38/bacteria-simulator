#include "SwarmBacterium.hpp"
#include "Swarm.hpp"
#include "Application.hpp"
#include "../Utility/Utility.hpp"
#include "../Random/Random.hpp"
#include <SFML/Graphics.hpp>
#include "NutrimentA.hpp"
#include "NutrimentB.hpp"
#include "Virus.hpp"

int SwarmBacterium::nb_swarm = 0;

// Constructeur_______________________________________________________________________________
SwarmBacterium::SwarmBacterium(Vec2d pos, Swarm* group)
    : Bacterium(uniform(getConfig()["energy"]["min"].toDouble(), getConfig()["energy"]["max"].toDouble()),
      pos, Vec2d::fromRandomAngle(),
      uniform(getConfig()["radius"]["min"].toDouble(), getConfig()["radius"]["max"].toDouble()),
      group->getStartColor()),
      swarm(group)
{
    swarm->addBacterium(this); // Ajout de la bactérie à son swarm
    ++ nb_swarm;// incrémente le compteur de bacteries
}

//Constructeur de copie_______________________________________________________________________
SwarmBacterium:: SwarmBacterium(SwarmBacterium const& bact)
    : SwarmBacterium(bact.getPosition(), bact.swarm)
{}

// Destructeur
SwarmBacterium::~SwarmBacterium()
{
    swarm->deleteBacterium(this); // Fonction qui enlève le pointeur de la bactérie détruite de son swarm
    -- nb_swarm;
}

// getter__________________________________________________________________________________________
MutableNumber SwarmBacterium::getProperty(const std::string& key) const
{
    auto paire = mutableParameters.find(key);
    if (key == paire->first)
         return paire->second;
    else return MutableNumber::positive(0, 0, 0, true, 0);
}

// Méthodes de mise à jour à chaque boucle________________________________________________________________________
void SwarmBacterium::drawOn(sf::RenderTarget& target) const
{
    Bacterium::drawOn(target);
}


// Méthodes d'évolution____________________________________________________________________________________

Bacterium* SwarmBacterium::clone() const
{
    SwarmBacterium* clone (new SwarmBacterium(*this));
    clone->consumeEnergy(energy_/2);
    clone->mutate(); // Possibilité de mutation
    clone->setAttackers(nullptr); // Clone n'a pas d'attaquant dès sa création
    clone->direction_ = -direction_; // direction inversée
    return clone;
}


void SwarmBacterium::move(sf::Time dt)
{
    // Récupération des valeurs necessaires
    Vec2d pos = getPosition();
    Vec2d speed = getSpeedVector();
    Quantity factor = getConsEnergy();

    // Resolution de l'équation différentielle
    DiffEqResult resultat = stepDiffEq(pos, speed, dt, *this);
    Vec2d new_pos = resultat.position;

    // Modifications sur la bacterie
    Quantity longueur_move = (new_pos - pos).length();
    if (pow(longueur_move, 2) >= 0.001) // on vérifie que le déplacement n'est pas négligeable
    {
        // La position change (déplacement)
        CircularBody::move(new_pos - pos);

        // La direction du leader change
        if (this == swarm->getLeader()) {
            // On garde la direction qui donne le meilleur score parmi 20 tirages aléatoires
            Vec2d direction = Vec2d::fromRandomAngle();
            Vec2d direction_aleatoire;
            for (int i(1); i<20; ++i)
            {
                direction_aleatoire = Vec2d::fromRandomAngle();
                if (getAppEnv().getPositionScore(getPosition() + direction_aleatoire)
                        > getAppEnv().getPositionScore(getPosition() + direction) )
                {
                    direction = direction_aleatoire;
                }
            }
            direction_ = direction;
        }

        // Baisse de niveau d'énergie
        consumeEnergy(longueur_move*factor);
     }

}

// Méthodes d'accès aux données________________________________________________________________________
j::Value const& SwarmBacterium::getConfig() const
{
    return (getAppConfig()["swarm bacterium"]);
}

Vec2d SwarmBacterium::f(Vec2d position, Vec2d speed) const
{
    double coeff = swarm->getConfig()[swarm->getId()]["force factor"].toDouble();
    Vec2d pos_leader = swarm->getPosLeader();

    return coeff* (pos_leader - position); // force exercée par le leader sur les bactéries du swarm
}

Vec2d SwarmBacterium::getSpeedVector() const
{
    // Retourne le vecteur vitesse qui vaut la direction multipliée par un coefficient
    double coeff = getConfig()["speed"]["initial"].toDouble();
    return coeff*direction_;
}

// Méthodes spécifiques à la nutrition des bactéries___________________________________________________________
Quantity SwarmBacterium::eatableQuantity(NutrimentA& nutriment)
{
    return nutriment.eatenBy(*this); // Nutrition adaptée au type de nutriment et de bactérie
}

Quantity SwarmBacterium::eatableQuantity(NutrimentB& nutriment)
{
   return nutriment.eatenBy(*this);  // Nutrition adaptée au type de nutriment et de bactérie
}

// Méthodes d'extension________________________________________________________________________________________
bool SwarmBacterium::isVulnerable() const
{
    return false;
}
