#pragma once
#include "Bacterium.hpp"
#include "Grip.hpp"

enum Etats { IDLE, WAIT_TO_DEPLOY, DEPLOY, ATTRACT, RETRACT, EAT };

class TwitchingBacterium: public Bacterium
{
public:
    // Constructeur
    TwitchingBacterium(Vec2d center);

    // destructeur
    virtual ~TwitchingBacterium();

    // getter
    virtual MutableNumber getProperty(const std::string& key) const override;  // trouve la propriété mutable associée à key

    // Méthode pour faciliter l'accès aux données
    virtual j::Value const& getConfig() const override;  // renvoi à la partie TwitchingBacterium du fichier de configuration
    virtual Quantity getConsEnergy ()const override;     // facteur de consommation d'énergie de la bactérie
    Quantity getTentaculeCons ()const;                   // facteur de consommation d'énergie du tentacule

    //Méthodes de mise à jour à chaque boucle
    virtual void drawOn(sf::RenderTarget& target) const override;  // dessin

    //Méthode d'évolution des TwitchingBacterium
    virtual void move(sf::Time dt) override;                       // déplacement
    void moveGrip(const Vec2d& delta);                             // Déplacement du grapin
    virtual Bacterium* clone()const override;                      // clonage

    // Méthodes spécifiques à la nutrition des bactéries
    virtual Quantity eatableQuantity(NutrimentA& nutriment) override ;    // Fait appel à la méthode eatenBy(SwarmBacterium& bact) de nutrimentA
    virtual Quantity eatableQuantity(NutrimentB& nutriment) override;     //Fait appel à la méthode eatenBy(SwarmBacterium& bact) de nutrimentB

    // Compteur de bacteries
    static int nb_twitch;

    // Méthodes d'extension
    virtual bool isVulnerable()const override;     // détermine si la bactérie est vulnérable à une attaque par une killer

private:
    Grip grapin;
    Etats Etat ;  // état de la bactérie parmi les types énumérés
};


