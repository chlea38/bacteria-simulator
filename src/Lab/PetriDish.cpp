#include "PetriDish.hpp"
#include "../Utility/Utility.hpp"
#include "Application.hpp"
#include "Bacterium.hpp"
#include "Nutriment.hpp"
#include "Swarm.hpp"
#include <cmath>


// Constructeur________________________________________________________________________________________________
PetriDish::PetriDish(Vec2d pos, double radius)
    :CircularBody(pos,radius),
     temperature(getAppConfig()["petri dish"]["temperature"]["default"].toDouble()),
     power ((getAppConfig()["petri dish"]["gradient"]["exponent"]["max"].toDouble()+getAppConfig()["petri dish"]["gradient"]["exponent"]["min"].toDouble())/2)
{}


// Destructeur_________________________________________________________________________________________________
PetriDish::~PetriDish()
{
    // Supprimer les bactéries et nutriments de l'assiette ainsi que son échantillonage
    reset();
}


//Getter_______________________________________________________________________________________________________
double PetriDish:: getTemperature()const
{
    return temperature;
}

Nutriment* PetriDish::getNutrimentColliding (const CircularBody& body ) const
{
    for (auto nutriment: nutriments_)
    {
        //recherche d'une éventuelle collision avec chaque nutriment de nutriments
        if (nutriment->isColliding(body))
        {
            return nutriment;
        }
    }
    return nullptr;
}

double PetriDish::getPositionScore (const Vec2d& p) const
{
    //Initialisation du score à 0
    double score=0;

    //Calcul du score en fonction des nutriments
    for(auto nutriment: nutriments_)
    {
        score+= nutriment->getRadius()/pow(distance(p, nutriment->getPosition()), power);
    }
    return score;
}

double PetriDish::getGradientExponent() const
{
    return power;
}

Swarm* PetriDish::getSwarmWithId(std::string id) const
{
    // Recherche du swarm correspondant à l'ID donné
    for (auto swarm : swarms) {
        if (swarm->getId() == id) {
            return swarm;
        }
    }
    return nullptr;
}

int PetriDish::getNutrimentNumber() const
{
    return nutriments_.size();
}

std::vector<Bacterium*> PetriDish::getBacterium() const
{
    return bacteries_;
}

//setter_______________________________________________________________________________________________________
void PetriDish::setTemperature(double const& t)
{
    temperature=t;
}

void PetriDish::setGradientExponent(double const& exponent)
{
    power = exponent;
}

// Methodes de mise à jour à chaque boucle_____________________________________________________________________
void PetriDish::update(sf::Time dt)
{
    // Fusion des nouveaux clones à l'ensemble de bactéries
    append(clones, bacteries_);

    // Suppresion des clones de l'ensemble clones
    for (auto& clone :clones)
    {
        clone=nullptr;
    }
    clones.clear();

    //Mise à jour de chaque nutriment de la boîte de pétri
    for(auto& nutriment: nutriments_)
    {
        nutriment->update(dt);
        if(nutriment->getRadius()==0)
        {
            // Suppression et désallocation des pointeurs de nutriments sans énergie
            delete nutriment;
            nutriment=nullptr;
        }
    }
    nutriments_.erase(std::remove(nutriments_.begin(), nutriments_.end(), nullptr), nutriments_.end());

    //Mise à jour de chaque bacteries de la boîte de pétri
    for(auto& bacterie: bacteries_)
    {
        bacterie->update(dt);
        if(bacterie->isDead()) {
            takeOutOfCase(bacterie->getCase(),bacterie);// enlève la bactérie de sa case
            delete bacterie;
            bacterie=nullptr;
        }
    }

   bacteries_.erase(std::remove(bacteries_.begin(), bacteries_.end(), nullptr), bacteries_.end());

   //Mise à jour de chaque swarm de la boîte de pétri
   for (auto& swarm: swarms)
   {
       swarm->update(dt);
   }
}


void PetriDish::drawOn(sf::RenderTarget& targetWindow) const
{
    // Dessiner un cercle noir d'epaisseur 5
    auto border = buildAnnulus(getPosition(), getRadius(), sf::Color::Black, 5);
    targetWindow.draw(border);

    //Dessiner chaque nutriment, bactérie et swarm de la boite de pétri
    for(auto& nutriment: nutriments_)
    {
        nutriment->drawOn(targetWindow);
    }

    for(auto& bacterie: bacteries_)
    {
       bacterie->drawOn(targetWindow);
    }
    for(auto& swarm: swarms)
    {
        swarm->drawOn(targetWindow);
    }
}



//Méthodes d'ajout/suppression _________________________________________________________________
bool PetriDish::addBacterium(Bacterium* bacterie)
{
    if (contains(*bacterie)) //Test pour ne pas que la bactérie dépasse de la boite de pétri
    {
      bacteries_.push_back(bacterie);
      addBacteriumToTab(bacterie); // Ajout de la bactérie à sa case
      return true;
    }
    else
    {
        // Suppression du pointeur créé comme il ne peut pas être ajouté à l'ensemble
        delete bacterie;
        bacterie=nullptr;
    }
    return false;
}

bool PetriDish::addClone(Bacterium* clone)
{
    if (contains(*clone)) //Test pour ne pas que la bactérie dépasse de la boite de pétri
    {
      clones.push_back(clone);
      addBacteriumToTab(clone); // Ajout du clone à sa case
      return true;
    }
    else
    {
        delete clone;
        clone=nullptr;
    }
    return false;
}


bool PetriDish::addNutriment(Nutriment* nutriment)
{
   if (contains(*nutriment)) //Test pour ne pas que le nutriment dépasse de la boite de pétri
   {
     nutriments_.push_back(nutriment);
     return true;
   }
   else
   {
       delete nutriment;
       nutriment=nullptr;
   }
   return false;
}


void PetriDish::reset()
{
     //On vide le quadrillage de ses pointeurs
    for (auto& ligne : cases)
    {
        for (auto& case_ : ligne)
        {
            for (auto& pointeur: case_)
            {
                // pas de delete car la propriété appartient à PetriDish
                pointeur=nullptr;
            }
            case_.clear();
        }
    }

    // On désalloue la mémoire des pointeurs
    for (auto& nutriment : nutriments_ )
    {
        delete nutriment;
        nutriment=nullptr;
    }

    for (auto& bacterie : bacteries_ )
    {
        delete bacterie;
        bacterie=nullptr;
    }  

    //On vide les tableaux de bacteries et de nutriments
    nutriments_.clear();
    bacteries_.clear();


}

void PetriDish::addSwarm(Swarm* swarm)
{
    swarms.push_back(swarm);
}


// Méthodes d'extensions_______________________________________________________________________________________
void PetriDish::addBacteriumToTab(Bacterium* bact)
{
    // Ajout de la bactérie dans la case correspondant à sa position
   bact->getCase().push_back(bact);
}

Case& PetriDish::getCase(int const& x, int const& y)
{
    return cases[x][y];
}

void PetriDish::takeOutOfCase(Case& c, Bacterium* bact)
{
    // Recherche de la bactérie dans le quadrillage et suppression dans celui-ci
            for (auto& bacterie : c)
            {
                 if (bacterie==bact)
                 {
                     bacterie=nullptr;
                 }
              }
              c.erase(std::remove(c.begin(), c.end(), nullptr), c.end());
}

