#pragma once
#include "CircularBody.hpp"
#include "../Utility/Types.hpp"
#include "../Utility/Vec2d.hpp"
#include "../Interface/Drawable.hpp"
#include "../Interface/Updatable.hpp"
#include<SFML/Graphics.hpp>

class Bacterium;
class SimpleBacterium;
class TwitchingBacterium;
class SwarmBacterium;
class KillerBacterium;

class Nutriment: public CircularBody, public Drawable, public Updatable
{
public:
    //Constructeur
    Nutriment(Quantity quantity, Vec2d center);

    //Destructeur
    virtual ~Nutriment();

    //Méthodes de mise à jour à chaque boucle
    virtual void drawOn(sf::RenderTarget& target) const override;   // dessin
    virtual void update(sf::Time dt) override;                      // mise à jour

    //Méthode de modification des Nutriments
    Quantity takeQuantity(Quantity const& qt);     // prélève la quantité de nutriment qt (ou le maximum disponible) et retourne la quantité qui a pu être prélevée
    void setQuantity (Quantity const&);            // change la quantité de nutriment

    // Méthode spécifiques à la nutrition des bactéries
    virtual Quantity eatenBy(Bacterium& bact) = 0;              // Oriente sur la méthode eatableQuantity de la bonne sous-classe de bactérie
    virtual Quantity eatenBy(SimpleBacterium& bact) = 0;        // Nutrition adaptée au type de nutriment ET de bactérie
    virtual Quantity eatenBy(TwitchingBacterium& bact) = 0;
    virtual Quantity eatenBy(SwarmBacterium& bact) = 0;
    virtual Quantity eatenBy(KillerBacterium& bact) =0;

    //Méthode pour faciliter accès aux données
    virtual j::Value const& getConfig() const =0;   // permet l'accès aux valeurs du fichier de configuration de Nutriment

protected:
    Quantity quantity_;  // quantité de nutriment
};
