#pragma once
#include "CircularBody.hpp"
#include "../Interface/Drawable.hpp"
#include "../Interface/Updatable.hpp"
#include <SFML/Graphics.hpp>
#include <vector>

class Swarm;
class Bacterium;
class Nutriment;

typedef std::vector<Bacterium*> Case;

class PetriDish : public CircularBody, public Drawable, public Updatable
{
public:
    // Constructeurs
    PetriDish(Vec2d pos, double radius);
    PetriDish(PetriDish const&)=delete;

    // Destructeur
    ~PetriDish();

    // Operateurs
    PetriDish& operator=(PetriDish const&) =delete;

    //Getter
    double getTemperature()const;                                         // température de l'assiette de pétri
    double getGradientExponent() const;                                   // gradient de l'environnement
    Nutriment* getNutrimentColliding (const CircularBody& body) const ;   // Retourne le nutriment en contact avec body/ nullptr sinon
    double getPositionScore (const Vec2d&) const;                         // Retourne le score associé à une position
    Swarm* getSwarmWithId(std::string id) const;                          // retrouve le swarm correspondant à l'identifiant id
    int getNutrimentNumber() const;                                       // retourne le nombre de nutriments dans l'assiette de pétri
    std::vector<Bacterium*> getBacterium() const;                         // retourne l'ensemble de bactéries présentes dans l'assiette de pétri

    //Setter
    void setTemperature(double const&);                 // change la température de l'assiette
    void setGradientExponent(double const& exponent);   // change le gradient

    //Méthodes de mise à jour à chaque boucle
    void update(sf::Time dt)  override;                            // mise à jour de l'assiette
    void drawOn(sf::RenderTarget& targetWindow) const  override;   // dessin de l'assiette

    //Méthodes d'ajout/suppression
    bool addBacterium(Bacterium*);     // ajoute une bactérie à l'ensemble de bactérie
    void addSwarm(Swarm* swarm);       // ajoute un swarm à l'ensemble de swarms
    bool addClone(Bacterium*);         // lorsqu'une bactérie se divise, ajoute le clone dans l'ensemble de clone
    bool addNutriment(Nutriment*);     // ajoute un nutriment dans l'ensemble de nutriment
    void reset();                      // Supprime les bactéries et nutriments de l'assiette (extension: et détruit le quadrillage)

    // Méthodes d'extensions
    void addBacteriumToTab(Bacterium* bact);            // Ajoute la bactérie à l'ensemble de case (cases), dans la case dont les coordonnées correspondent aux coordonées spatiales de sa position
    Case& getCase(int const& x, int const& y);          //Retourne la case de coordonnées x et y
    void takeOutOfCase(Case& c, Bacterium* bact);       // Enlève une bactérie donnée de l'ensemble de cases

private:
    double temperature;                      // température de l'assiette
    double power;                            // puissance de l'attraction (gradient)
    std::vector<Nutriment*> nutriments_;     // ensemble de nutriments
    std::vector<Bacterium*> bacteries_;      // ensemble de bactéries
    std::vector<Bacterium*> clones;          // ensemble de clones, ensuite ajouté à l'ensemble de bactéries
    std::vector<Swarm*> swarms;              // ensemble de swarms
    std::array<std::array<Case,ECHANTILLONAGESIZE>,ECHANTILLONAGESIZE> cases; // quadrillage de la petri dish
};

