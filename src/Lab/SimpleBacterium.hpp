#pragma once
#include "Bacterium.hpp"
#include "../Utility/DiffEqSolver.hpp"

class SimpleBacterium: public Bacterium, public DiffEqFunction
{
public:
    // Constructeurs
    SimpleBacterium(Quantity energy, Vec2d center,Vec2d direction, double radius, MutableColor color);
    SimpleBacterium(Vec2d center);

    //Destructeur
    virtual ~SimpleBacterium();

    // getter
    virtual MutableNumber getProperty(const std::string& key) const override;  // retourne le paramètre mutable associé à key

    // Méthode pour faciliter l'accès aux données
    virtual j::Value const& getConfig() const override;                  // accès à la partie simpleBacterium du fichier de configuration

    // Méthode permettant la division
    virtual Bacterium* clone()const override;                            // clonage de la bactérie

    // Méthode permettant le déplacement
    virtual Vec2d f(Vec2d position, Vec2d speed) const override;         // Calcul de la force
    Vec2d getSpeedVector() const;                                        // calcule le vecteur vitesse à partir de la direction et d'un coefficient (speed)
    virtual void move(sf::Time dt) override;                             // déplacement

    // Méthodes spécifiques à la nutrition des bactéries
    virtual Quantity eatableQuantity(NutrimentA& nutriment) override ;   // Fait appel à la méthode eatenBy(SimpleBacterium& bact) de nutrimentA
    virtual Quantity eatableQuantity(NutrimentB& nutriment) override;    // Fait appel à la méthode eatenBy(SimpleBacterium& bact) de nutrimentB

    // Méthodes de mises à jour à chaque boucle
    virtual void drawOn(sf::RenderTarget& target) const override;        // dessin

    // Compteur de bacteries
    static int nb_simple;

    // Méthodes d'extension
    virtual bool isVulnerable() const override; // détermine si la bactérie est vulnérable à une attaque par une killer

private:
    double t_;                                  // compteur permettant le mouvement du flagelle
};

