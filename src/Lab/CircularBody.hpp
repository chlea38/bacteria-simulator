#pragma once
#include "../Utility/Vec2d.hpp"
#include<iostream>

class CircularBody
{
public:
    //Getters
    Vec2d getPosition() const;   // retourne la position du circular body
    double getRadius() const;    // retourne le rayon

    //Setters
    void setRadius(const double& radius);   // change le rayon
    void setPosition(const Vec2d& center);  // change la position

    //Methodes
    void move(const Vec2d& center);                     // déplacement
    bool contains(const CircularBody& other) const;     // Vérifie si le CircularBody other est contenu dans l'instance courante
    bool isColliding(const CircularBody& other) const;  // Vérifie la collision entre deux CircularBody
    bool contains(const Vec2d& point) const;            // Verifie si point est contenu dans le CircularBody

protected:
    // constructeurs
    CircularBody(Vec2d centre, double radius);
    CircularBody(const CircularBody& other);

    //destructeur
    virtual ~CircularBody();

    //Operateur interne
    CircularBody& operator=(CircularBody const& other);

private:
    // Attributs
    Vec2d center_;   // position du centre
    double radius_;  // rayon
};

//Operateurs externes
bool operator>(const CircularBody& body1, const CircularBody& body2);        //Verifie si body 2 est contenu dans body 1
bool operator&(const CircularBody& body1, const CircularBody& body2);        // Verifie si body 1 et body 2 sont en collision
bool operator>(const CircularBody& body1, const Vec2d& point);               // Verifie si point est contenu dans body 1
std::ostream& operator<<(std::ostream& out, const CircularBody& body);       //Affiche la position et le rayon du CircularBody
