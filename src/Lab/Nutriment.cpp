#include "Nutriment.hpp"
#include "Application.hpp"
#include "../Utility/Utility.hpp"
#include "Bacterium.hpp"
#include<string>
#include<cmath>

//Constructeur_________________________________________________________________________________________________
Nutriment::Nutriment(Quantity quantity, Vec2d center)
: CircularBody(center, quantity),
  quantity_(quantity)
{}


//Destructeur__________________________________________________________________________________________________
Nutriment::~Nutriment()
{}


//Méthodes de mise à jour à chaque boucle______________________________________________________________________
void Nutriment::drawOn(sf::RenderTarget& target) const
{
    //Choix de la hauteur pour le texte
    Vec2d position (0, - getRadius() -15);

    //Dessin d'un nutriment
    auto const& texture = getAppTexture(this->getConfig()["texture"].toString());
    auto nutrimentSprite = buildSprite(getPosition(), 6, texture);
    nutrimentSprite.setScale(2 * getRadius() / texture.getSize().x, 2 * getRadius() / texture.getSize().y);
    target.draw(nutrimentSprite);

    //Affichage en mode debug : affiche quantité disponible
    if(isDebugOn())
    {
    //Valeur du rayon (arrondie à l'unité)
    int quantite(floor(quantity_));

    //Dessin du texte
    auto const text = buildText(std::to_string(quantite), getPosition()+position ,getAppFont(),15,sf::Color::Black);
    target.draw(text);
    }
}


void Nutriment::update(sf::Time dt)
{
    //Récupération des différentes paramètres nécessaires
    auto const& speed (getConfig()["growth"]["speed"].toDouble());
    auto const& minTemp=getConfig()["growth"]["min temperature"].toDouble();
    auto const& maxTemp=getConfig()["growth"]["max temperature"].toDouble();
    auto const& maxSize=getConfig()["quantity"]["max"].toDouble()*2;
    double temp(getAppEnv().getTemperature());

    //Calcul facteur de croissance du nutriment
    auto growth = speed * dt.asSeconds();

    // Croissance du nutriment si les conditions sont respectées
    if((temp<=maxTemp and temp>=minTemp)) //Température optimale
   {
     setQuantity(std::min(quantity_+=growth,maxSize)); // Simulation de croissance
     // Vérification que le nutriment ne dépasse pas de l'assiette de pétri si il a une taille supérieure
     if (!getAppEnv().contains(*this))
        {
        setQuantity(quantity_-=growth); // Sinon retour à sa taille précédente
        }
    }
}


//Méthode de modification des Nutriments_______________________________________________________________________
Quantity Nutriment:: takeQuantity(Quantity const& q)
{
    //On détermine quelle est la quantité qui sera prelevée : au maximum ce qu'on a
    Quantity prelevement=std::min(quantity_, q);

    //On remet a jour la quantite de nutriment après prélevement
    setQuantity(quantity_-prelevement);
    return prelevement;
}


void Nutriment::setQuantity (Quantity const& q){
    //On ne peut pas attribuer une valeur négative
    quantity_=std::max(0.0,q);

    //Adapter le rayon à la quantité de nutriments
    setRadius(quantity_);
}







