#include "TwitchingBacterium.hpp"
#include "../Random/Random.hpp"
#include "../Utility/Utility.hpp"
#include "NutrimentA.hpp"
#include "NutrimentB.hpp"
#include "Virus.hpp"

int TwitchingBacterium::nb_twitch = 0;

// Constructeur________________________________________________________________________________________________
TwitchingBacterium::TwitchingBacterium(Vec2d center)
    : Bacterium(uniform(getConfig()["energy"]["min"].toDouble(), getConfig()["energy"]["max"].toDouble()),
      center, Vec2d::fromRandomAngle(),
      uniform(getConfig()["radius"]["min"].toDouble(), getConfig()["radius"]["max"].toDouble()),
      getConfig()["color"]),
      grapin(center, getRadius()/4),
      Etat (IDLE)
{

    addProperty("tentacle length", MutableNumber::positive(getConfig()["tentacle"]["length"],false,0));
    addProperty("tentacle speed", MutableNumber::positive(getConfig()["tentacle"]["speed"], false, 0));
    ++nb_twitch;// incrémente le compteur de bacteries
}

// Destructeur
TwitchingBacterium::~TwitchingBacterium()
{
    --nb_twitch;
}

// getter___________________________________________________________________________________________________
MutableNumber TwitchingBacterium::getProperty(const std::string& key) const
{
    auto paire = mutableParameters.find(key);
    if (key == paire->first)
         return paire->second;
    else return MutableNumber::positive(0, 0, 0, true, 0);
}

// Méthode pour faciliter l'accès aux données__________________________________________________________________
j::Value const& TwitchingBacterium:: getConfig() const
{
    return (getAppConfig()["twitching bacterium"]);
}

Quantity TwitchingBacterium:: getConsEnergy ()const
{
     return getConfig()["energy"]["consumption factor"]["move"].toDouble();
}

Quantity TwitchingBacterium::getTentaculeCons ()const
{
     return getConfig()["energy"]["consumption factor"]["tentacle"].toDouble();
}

//Méthodes de mise à jour à chaque boucle______________________________________________________________________
void TwitchingBacterium::drawOn(sf::RenderTarget& target) const
{
    // Dessin de la bacterie grâce à drawOn de Bacterium
    Bacterium::drawOn(target);

    // Dessin du tentacule
    sf::Color couleur (color_.get());   // Récupération de la couleur de la bactérie
    auto line = buildLine(this->getPosition(), grapin.getPosition(),couleur , 2);

    target.draw(line);
    // Dessin du grapin
    auto const rond = buildCircle(grapin.getPosition(), grapin.getRadius(), couleur);
    target.draw(rond);

}

//Méthode d'évolution des TwitchingBacterium___________________________________________________________________
void TwitchingBacterium::move(sf::Time dt)
{
    // Récupération des valeurs utiles
   auto const& vitesse_tentacule (getProperty("tentacle speed").get());
   auto const& facteur_vitesse (getConfig()["speed factor"].toDouble());
   Vec2d direction_tentacule ((grapin.getPosition()-getPosition()).normalised());
   Vec2d direction_retraction ((getPosition()-grapin.getPosition()).normalised());

   switch(Etat) // test de chaque état possible
    {
    case IDLE:
        Etat=WAIT_TO_DEPLOY; // Passe juste à l'état suivant
        break;

   case WAIT_TO_DEPLOY:
   {
       // On garde la direction qui donne le meilleur score parmi 20 tirages aléatoires
       Vec2d direction (Vec2d::fromRandomAngle());
       Vec2d direction_aleatoire;
       for (int i(1); i<20; ++i)
       {
           direction_aleatoire = Vec2d::fromRandomAngle();
           if (getAppEnv().getPositionScore(getPosition() + direction_aleatoire) > getAppEnv().getPositionScore(getPosition() + direction))
           {
               direction = direction_aleatoire;
           }
       }
       direction_=direction; // On assigne cette direction à la bactérie
       Etat=DEPLOY; // Passage état suivant
       break;
   }

    case DEPLOY:
    {
        // Calcul de la nouvelle position du tentacule
        Vec2d newPosition(direction_*vitesse_tentacule*dt.asSeconds());

        // Déploiement du tentacule si celui si ne dépasse pas la longueur maximale et reste dans l'assiette
        if (distance(this->getPosition(), grapin.getPosition()+newPosition)<=getProperty("tentacle length").get()
             and getAppEnv().contains(grapin))
        {
            moveGrip(newPosition);
            consumeEnergy(getTentaculeCons()*vitesse_tentacule*dt.asSeconds()); // consommation d'énergie due au déploiement
        }
        else Etat=RETRACT; // Rétractation du tentacule s'il a atteint sa longueur maximale sans rencontrer de nutriment
        if (getAppEnv().getNutrimentColliding(grapin)!=nullptr) Etat=ATTRACT; // Attractiond de la bactérie si le grapin rencontre un nutriment
        break;
    }

    case ATTRACT:
        if (getAppEnv().getNutrimentColliding(*this)!=nullptr) Etat=EAT; // bactérie en contact avec le nutriment : se nourrit
        else
        {
            if (getAppEnv().getNutrimentColliding(grapin)!=nullptr) // bactérie n'est pas en contact avec le nutriment : s'en rapproche
            {
                this->CircularBody::move(direction_tentacule*vitesse_tentacule*facteur_vitesse*dt.asSeconds()); // rapprochement vers le nutriment, en direction du tentacule
                consumeEnergy(vitesse_tentacule*facteur_vitesse*dt.asSeconds()*getConsEnergy());    // consommation d'énergie liée au déplacement
            }
            else Etat=RETRACT; // Cas ou le nutriment a "disparu" entre temps : tentacule se rétracte
        }
        break;

    case RETRACT:
        if (distance(getPosition(), grapin.getPosition())<=getRadius()) Etat=IDLE; // Le tentacule est totalement retracté : retour à l'état initial
        else
        {
            Vec2d changePosition(direction_retraction*vitesse_tentacule*dt.asSeconds());
            moveGrip(changePosition);   // rétractation du tentacule en direction de la bactérie
            consumeEnergy(getTentaculeCons()*vitesse_tentacule*dt.asSeconds());  // consommation d'énergie liée au déplacement du tentacule
        }
        break;

    case EAT:
        if (getAppEnv().getNutrimentColliding(*this)==nullptr) Etat=IDLE; // retour à l'état initial quand le nutriment à été consommé
        break ;
  }
}

Bacterium* TwitchingBacterium:: clone()const
{
    //Création du clone
    TwitchingBacterium* clone (new TwitchingBacterium(*this));
    clone->consumeEnergy(energy_/2);
    clone->direction_ = -direction_; // direction inversée
    clone->setAttackers(nullptr); // Clone n'a pas d'attaquant dès sa création

    // Déplacement léger du clone pour pouvoir observer le clonage
    Vec2d deltaPosition (15,15);
    clone->setPosition(getPosition()+deltaPosition);

    //Le grapin doit être rétracté à la création de la TwitchingBacterium
    clone->grapin.setPosition(clone->getPosition());

    clone->mutate(); // mutation
    ++nb_twitch; // incrémente le compteur
    return clone;
}

void TwitchingBacterium ::moveGrip(const Vec2d& delta)
{
    grapin.move(delta);
}

// Méthodes spécifiques à la nutrition des bactéries___________________________________________________________
Quantity TwitchingBacterium::eatableQuantity(NutrimentA& nutriment)
{
    return nutriment.eatenBy(*this); // Nutrition adaptée au type de nutriment et de bactérie
}

Quantity TwitchingBacterium::eatableQuantity(NutrimentB& nutriment)
{
   return nutriment.eatenBy(*this); // Nutrition adaptée au type de nutriment et de bactérie
}

// Méthodes d'extension________________________________________________________________________________________
bool TwitchingBacterium::isVulnerable() const
{
    return false;
}

