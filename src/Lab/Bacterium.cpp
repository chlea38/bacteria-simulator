#include "Bacterium.hpp"
#include "Nutriment.hpp"
#include "../JSON/JSON.hpp"
#include "../Utility/Utility.hpp"
#include<cmath>
#include "Virus.hpp"


//Constructeur_________________________________________________________________________________________________
Bacterium::Bacterium(Quantity energy, Vec2d center, Vec2d direction, double radius, MutableColor color)
    :CircularBody(center, radius), direction_(direction),  color_(color),angleDirectionRad(direction.angle()),
       tBasculement(sf::Time::Zero), energy_(energy),attackers(nullptr), abstinence_(false), compteur(sf::Time::Zero)
{
    if ( getPosition().x<=getApp().getLabSize().x and getPosition().x>=0 and getPosition().y<=getApp().getLabSize().x and getPosition().y>=0)
        case_={floor(getPosition().x*(ECHANTILLONAGESIZE/getApp().getLabSize().x)), floor(getPosition().y*(ECHANTILLONAGESIZE/getApp().getLabSize().y))};
}


//Destructeur__________________________________________________________________________________________________
Bacterium::~Bacterium()
{
    // Destruction du virus si la bactérie était attaquée
    if (attackers!=nullptr)
    {
    delete attackers;
    attackers=nullptr;
    }
}

//Getter_______________________________________________________________________________________________________
MutableNumber Bacterium::getProperty(const std::string& key) const
{
   auto paire = mutableParameters.find(key);
   return paire->second;
}

//Méthodes de mise à jour à chaque boucle______________________________________________________________________
void Bacterium:: drawOn(sf::RenderTarget& target) const
{
    //Affichage de la bactérie
    sf::Color color (color_.get());   // Récupération de la couleur de la bactérie
    auto const circle = buildCircle(getPosition(), getRadius(), color);
    target.draw(circle);

    //Choix de la hauteur pour le texte
    Vec2d position (0, - getRadius() -15);

    //Affichage énergie si mode debug activé
    if(isDebugOn())
    {
    //Valeur de l'énergie (arrondie à l'unité)
    int energie(floor(energy_));

    //Dessin du texte
    auto const text = buildText(std::to_string(energie), getPosition()+position, getAppFont(),15, sf::Color::Red);
    target.draw(text);
    }

    // Dessin de son virus si elle est attaquée
    if(isTarget()) attackers->drawOn(target);
}

void Bacterium::update(sf::Time dt)
{
    //Mise à jour des compteur
    compteur+=dt;
    tBasculement+=dt;

    // Bactérie se déplace
    move(dt);

    // Mise à jour du virus de la Bactérie
    updateAttackers(dt);

    // Changement de direction si la bactérie rencontre le bord de l'assiette
    if (getAppEnv().doesCollideWithDish(*this) )
    {
        // La direction est inversée
         direction_=-direction_;
    }

    // On met à jour l'angle de direction
     auto const angleDiff = angleDelta(direction_.angle(), angleDirectionRad); // calcule la différence entre le nouvel                                                                    // angle de direction et l'ancien
     auto dalpha = M_PI * compteur.asSeconds();    // calcule dα
     dalpha = std::min(dalpha, std::abs(angleDiff)); // on ne peut tourner plus que de angleDiff
     dalpha = std::copysign(dalpha, angleDiff); // on tourne dans la direction indiquée par angleDiff
     angleDirectionRad += dalpha; // angle de rotation mis à jour

    // Consommation de nutriment ou non
    if(getAppEnv().getNutrimentColliding(*this) !=nullptr  // La bactérie rencontre un nutriment
            and !abstinence_                      // Elle n'est pas abstinente
            and compteur>getTime())             // Elle ne s'est pas nourrie trop récemment
    {
        //Remise du compteur à zéro
        compteur=sf::Time::Zero;

        // Nutrition
          eat(*(getAppEnv().getNutrimentColliding(*this)));

        // Appel à la méthode division
        this->divide();
    }

    //Mise à jour de sa case
    updateCase();
}


//Méthode d'évolution des Bacterium____________________________________________________________________________

void Bacterium::divide()
{
    // On verifie si la bacterie a assez d'energie pour se diviser
    if (energy_ > getMinEnergy())
    {
        // On ajoute au vecteur de clones la nouvelle bacterie créée avec clone()
        getAppEnv().addClone(this->clone());
        energy_ /= 2; // energie divisée par 2
    }
}

void Bacterium:: mutate()
{
        for (auto& parametre : mutableParameters)
        {
            parametre.second.mutate();
        }
        color_.mutate();
}


bool Bacterium::isDead() const
{
    if(energy_<=0) return true;
    return false;
}

void Bacterium:: consumeEnergy (Quantity const& qt)
{
    // Diminution de l'énergie
    energy_=std::max(energy_-qt,0.0);
}

void Bacterium::addProperty(const std::string& key, MutableNumber nb)
{
    mutableParameters[key] = nb;
}

// Méthodes spécifiques à la nutrition des bactéries___________________________________________________________
void Bacterium::eat (Nutriment& nutriment)
{
    Quantity eaten(nutriment.eatenBy(*this)); // Retourne la quantité mangée et decrémente la quantité de nutriment
    energy_+=eaten;
}

//Méthode pour faciliter accès aux données_____________________________________________________________________
Quantity Bacterium::getMinEnergy() const
{
    // Energie minimum nécessaire pour la division
    return getConfig()["energy"]["division"].toDouble();
}

sf::Time Bacterium::getTime()const
{
    // Temps minimum nécessaire pour que la bactérie se nourisse à nouveau
    auto const& time=getConfig()["meal"]["delay"].toDouble();
    return sf::seconds(time);
}

Quantity Bacterium::getConsEnergy()const
{
    // Facteur de consommation d'énergie de la bactérie
    return getConfig()["energy"]["consumption factor"].toDouble();
}

Quantity Bacterium:: getMaxEatableQuantity() const
{
    //Quantité maximale de nutriment qu'une bactérie peut consommer
     return getConfig()["meal"]["max"].toDouble();
}

Case& Bacterium::getCase()
{
    // Retourne la case qui correspond à la position de la bactérie (coordonnées multipliées par le facteur d'échantillonage du lab)
    return getAppEnv().getCase((int)(getPosition().x*(ECHANTILLONAGESIZE/getApp().getLabSize().x)),(int)(getPosition().y*(ECHANTILLONAGESIZE/getApp().getLabSize().y)));
}


void Bacterium::setAttackers(Virus* virus)
{
    // Met à jour le virus par lequel la bactérie est attaquée
    attackers=virus;
}

bool Bacterium::isTarget() const
{
    // Retourne true si la bactérie est attaquée par un virus
    return (attackers!=nullptr);
}

void Bacterium::updateAttackers(sf::Time dt)
{
    if(attackers!=nullptr)
    {
        // Mise à jour du virus
        attackers->update(dt);
        // Suppression du virus s'il est mort
        if(attackers->isDead())
        {
            delete attackers;
            attackers=nullptr;
        }
    }
}

void Bacterium::isAttacked()
{
    consumeEnergy(getConfig()["energy"]["attack malus"].toDouble());
}

void Bacterium::updateCase()
{
    getAppEnv().takeOutOfCase(getAppEnv().getCase(case_.x,case_.y), this); // on enlève la bactérie de sa case
    case_={floor(getPosition().x*(ECHANTILLONAGESIZE/getApp().getLabSize().x)),floor(getPosition().y*(ECHANTILLONAGESIZE/getApp().getLabSize().y))}; // on calcule sa nouvelle case
    getAppEnv().addBacteriumToTab(this); // on la remet dans la bonne case
}
