#include "KillerBacterium.hpp"
#include "../Random/Random.hpp"
#include "../Utility/Utility.hpp"
#include <cmath>
#include "NutrimentA.hpp"
#include "NutrimentB.hpp"
#include "SimpleBacterium.hpp"
#include "Virus.hpp"

int KillerBacterium::nb_killer=0;

//Constructeurs_________________________________________________________________________________________________
KillerBacterium::KillerBacterium (Vec2d centre): SimpleBacterium(uniform(this->getConfig()["energy"]["min"].toDouble(), getConfig()["energy"]["max"].toDouble()),
    centre, Vec2d::fromRandomAngle(),
    uniform(getConfig()["radius"]["min"].toDouble(), getConfig()["radius"]["max"].toDouble()),
    this->getConfig()["color"]),
    target(nullptr),
    virus (new Virus (centre, this)) // Création d'un virus centré sur la bactérie et qui appartient à la Killer
{
    ++nb_killer;
}

KillerBacterium::KillerBacterium(Quantity energy, Vec2d center,Vec2d direction, double radius, MutableColor color)
    : SimpleBacterium(energy, center, direction, radius, color),
    target(nullptr)
{
    --nb_killer;
}

//Constructeur de copie_______________________________________________________________________
KillerBacterium::KillerBacterium(KillerBacterium const& bact)
    :KillerBacterium(bact.getPosition())
{}

KillerBacterium::~KillerBacterium()
{
    // Méthode qui désalloue les pointeurs
    reset();
}

void KillerBacterium:: setTarget()
{
        for(auto& bacterie: getCase()) // Parcours les bactéries de cette case
        {
            if (bacterie!=nullptr and bacterie!=this)
            {
                // La premiere bactérie trouvée dans la case, autre que elle-même devient la cible de la killer
               target=bacterie;
               break; // On peut alors sortir de la boucle
            }
        }

    // si aucune bactérie trouvée, target ne change pas (est déjà à nullptr)
}

void KillerBacterium::update(sf::Time dt)
{
    // Se met à jour comme une bactérie
    Bacterium::update(dt);
    // En plus, doit mettre à jour son virus s'il l'a encore
    if(virus!=nullptr) virus->update(dt);
}

void KillerBacterium:: move(sf::Time dt)
{
    setTarget(); // Mise à jour de la cible de la Killer

    // Vérifie qu'elle possède une cible, un virus et que la bactérie est aps déjà la cible d'un virus avant d'attaquer
    if (target!=nullptr and !target->isDead() and isVictim(target) and stillHasVirus() and !target->isTarget())
    {
       attack(target);
    }
    // Sinon, la killer se déplace normalement (comme une SimpleBacterium classique)
    else
    {
        SimpleBacterium::move(dt);
    }

    // Remise à zéro de sa cible
    target = nullptr;
}

void KillerBacterium::drawOn(sf::RenderTarget& target) const
{
    // Se dessine comme une SimpleBacterium (mais avec ses propres valeurs du fichier de configuration)
    SimpleBacterium::drawOn(target);

    // Dessine en plus son virus si elle en a un
    if (virus!=nullptr) virus->drawOn(target);

}

j::Value const& KillerBacterium::getConfig() const
{
    return (getAppConfig()["killer bacterium"]);
}


Bacterium* KillerBacterium::clone()const
{
    KillerBacterium* clone (new KillerBacterium(*this));
    clone->consumeEnergy(energy_/2);
    clone->direction_ = -direction_; // direction inversée
    clone->mutate(); // Possibilité de mutation
    ++nb_killer;// Augmente le nombre de bacterie
    return clone;
}

void KillerBacterium::attack (Bacterium* bact)
{
    // Consommation d'une quantité d'énergie liée à l'attaque
   consumeEnergy(getConfig()["energy"]["attack consumption"].toDouble());

       if(virus!=nullptr)
       {
           // Le virus prend connaissance de sa cible
           virus->setTarget(bact);

           // La bactérie prend connaissance de son attaquant
           bact->setAttackers(virus);

           // La Killer ne possède plus le virus qui a été transmis à bact
           virus=nullptr;
       }
}

bool KillerBacterium:: isVictim(Bacterium* bact) const
{
    if (bact!=nullptr)
   {
      // Double dispatch pour déterminer si une bactérie est vulnérable à l'attaque selon son type
      return bact->isVulnerable();
   }
    return false;
}

Quantity KillerBacterium:: eatableQuantity(NutrimentA& nutriment)
{
    return nutriment.eatenBy(*this); // Fait appel à la méthode eatenBy de nutrimentA sur le bon type de bactérie
}
Quantity KillerBacterium::eatableQuantity(NutrimentB& nutriment)
{
    return nutriment.eatenBy(*this); // Fait appel à la méthode eatenBy de nutrimentB sur le bon type de bactérie
}

bool KillerBacterium::isVulnerable() const
{
    return false;
}

bool  KillerBacterium::stillHasVirus() const
{
    // Retourne true si la killer possède toujours un virus
    if (virus!=nullptr) return true;
    return false;
}

void KillerBacterium::reset()
{
   // Destruction du virus si la KillerBacterium le porte toujours
   if (virus!=nullptr) delete virus;
   virus=nullptr;
   target=nullptr;
}
