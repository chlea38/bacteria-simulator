#pragma once
#include <vector>
#include <string>
#include <SFML/Graphics.hpp>
#include "Application.hpp"
#include "../Utility/MutableColor.hpp"

class SwarmBacterium;
class Swarm
{
public:
    // Constructeur
    Swarm(std::string id);

    // Destructeur
    ~Swarm();

    //Getter
    std::string getId() const;                     // retourne l'identifiant du swarm
    j::Value const& getConfig() const;             // renvoit à la partie swarm du fichier de configuration
    Vec2d getPosLeader() const;                    // retourne la position de la bactérie leader
    SwarmBacterium* getLeader() const;             // retourne un pointeur sur la bactérie leader
    MutableColor getStartColor() const;            // couleur initiale des bactéries du swarm

    // Methodes
    void update(sf::Time dt);                      // mise à jour du swarm
    void drawOn(sf::RenderTarget& target) const;   // dessin de l'anneau rouge autour du leader
    bool addBacterium(SwarmBacterium* bact);       // ajout d'une bactérie au swarm
    void deleteBacterium(SwarmBacterium* bact);    // suppression d'une bactérie du swarm

private:
    std::string id_;                               // identifiant du swarm
    std::vector<SwarmBacterium*> bacterium_;       // ensemble de bactéries du swarm
    SwarmBacterium* leader_;                       // bactérie leader
};
