#include "SimpleBacterium.hpp"
#include "../Random/Random.hpp"
#include "../Utility/Utility.hpp"
#include "NutrimentA.hpp"
#include "NutrimentB.hpp"
#include "Virus.hpp"

int SimpleBacterium::nb_simple =0;

// Constructeurs___________________________________________________________________________________________________________________
SimpleBacterium::SimpleBacterium(Quantity energy, Vec2d center,Vec2d direction, double radius, MutableColor color)
    : Bacterium(energy, center, direction, radius, color),
      t_(uniform(0., M_PI))
{
    addProperty("speed", MutableNumber::positive(getConfig()["speed"], false, 0));
    addProperty("tumble better", MutableNumber::positive(getConfig()["tumble"]["better"], false, 0));
    addProperty("tumble worse", MutableNumber::positive(getConfig()["tumble"]["worse"], false, 0));
    ++nb_simple; // Incrémente le compteur de bacteries
}

SimpleBacterium::SimpleBacterium(Vec2d centre)
    : Bacterium(uniform(this->getConfig()["energy"]["min"].toDouble(), getConfig()["energy"]["max"].toDouble()),
                centre, Vec2d::fromRandomAngle(),
                uniform(getConfig()["radius"]["min"].toDouble(), getConfig()["radius"]["max"].toDouble()),
                this->getConfig()["color"]),
     t_(uniform(0., M_PI))
{
    addProperty("speed", MutableNumber::positive(getConfig()["speed"], false, 0));
    addProperty("tumble better", MutableNumber::positive(getConfig()["tumble"]["better"], false, 0));
    addProperty("tumble worse", MutableNumber::positive(getConfig()["tumble"]["worse"], false, 0));
    ++ nb_simple;// Incrémente le compteur de bacteries
}

//Destructeur__________________________________________________________________________________________________
SimpleBacterium::~SimpleBacterium()
{
    --nb_simple;
}

//Getter___________________________________________________________________________________________________________

MutableNumber SimpleBacterium::getProperty(const std::string& key) const
{
   auto paire = mutableParameters.find(key);
   if (key == paire->first) return paire->second;
   else return MutableNumber::positive(0, 0, 0, true, 0);

}

// Méthode pour faciliter l'accès aux données_______________________________________________________________________________________
j::Value const& SimpleBacterium:: getConfig() const
{
    return (getAppConfig()["simple bacterium"]);
}


// Méthode permettant la division___________________________________________________________________________________________________
Bacterium* SimpleBacterium::clone()const
{
    SimpleBacterium* clone (new SimpleBacterium(*this));
    clone->consumeEnergy(energy_/2);
    clone->direction_ = -direction_; // direction inversée
    clone->mutate(); // Possibilité de mutation
    clone->setAttackers(nullptr); // Clone n'a pas d'attaquant dès sa création
    ++nb_simple;// Augmente le nombre de bacterie
    return clone;
}


// Méthodes permettant le déplacement___________________________________________________________________________________________________________
Vec2d SimpleBacterium::f(Vec2d position, Vec2d speed) const
{
    // Pour une bactérie simple la force est 0 (mouvement rectiligne uniforme)
    return {0,0};
}

Vec2d SimpleBacterium::getSpeedVector() const
{
    // Retourne le vecteur vitesse qui vaut la direction multipliée par un coefficient
    double coeff = getProperty("speed").get();
    return coeff*direction_;
}

void SimpleBacterium::move(sf::Time dt)
{
        // Récupération des valeurs necessaires
        Vec2d pos = getPosition();
        Vec2d speed = getSpeedVector();
        Quantity factor = getConsEnergy();
        double lastScore = getAppEnv().getPositionScore(pos); // score du pas de simulation précédent (avant mouvement)

        // Resolution de l'équation différentielle
        DiffEqResult resultat = stepDiffEq(pos, speed, dt, *this);
        Vec2d new_pos = resultat.position;
        Vec2d new_speed = resultat.speed;

        // Modifications sur la bacterie
        Quantity longueur_move = (new_pos - pos).length();
        if (pow(longueur_move, 2) >= 0.001) // on vérifie que le déplacement n'est pas négligeable
        {
            // La position change (déplacement)
            CircularBody::move(new_pos - pos);

            // La direction change: on divise le vecteur vitesse par la valeur dans getSpeedVector
            double coeff = getProperty("speed").get();
            direction_ = new_speed/coeff;

            // Baisse de niveau d'énergie
            consumeEnergy(longueur_move*factor);

            // Mise à jour du compteur t
            t_+= 3 * dt.asSeconds();
        }

        // Basculement ? -> true=la bactérie bascule
        double score = getAppEnv().getPositionScore(getPosition()); // score courant (après mouvement)
        double proba_basculement = 0;
        if (score>= lastScore) // Si le score s'améliore
        {
            double lambda = getProperty("tumble better").get();
            proba_basculement = 1 - exp(-tBasculement.asSeconds()/lambda);
        } else { // Si le score se péjore
            double lambda = getProperty("tumble worse").get();
            proba_basculement = 1 - exp(-tBasculement.asSeconds()/lambda);
        }
        bool basculement = bernoulli(proba_basculement);



        // Si la bactérie bascule-> basculement
        if (basculement) {
            // On remet le compteur de basculement à 0
            tBasculement = sf::Time::Zero;

            // On choisit un algorithme de basculement
            Vec2d direction = Vec2d::fromRandomAngle();
            if (getConfig()["tumble"]["algo"].toString() == "best of N")
            {
                // On garde la direction qui donne le meilleur score parmi 20 tirages aléatoires
                Vec2d direction_aleatoire;
                for (int i(1); i<=20; ++i)
                {
                    direction_aleatoire = Vec2d::fromRandomAngle();
                    if (getAppEnv().getPositionScore(getPosition() + direction_aleatoire) > getAppEnv().getPositionScore(getPosition() + direction) )
                    {
                        direction = direction_aleatoire;
                    }
                }
            }
            // Si le fichier de config contient "single random vector" alors la direction est simplement le Vec2d direction ci dessus

            // On change la direction avec le résultat de l'algorithme choisi
            direction_ = direction;
        }
}

// Méthodes spécifiques à la nutrition des bactéries___________________________________________________________
Quantity SimpleBacterium::eatableQuantity(NutrimentA& nutriment)
{
    return nutriment.eatenBy(*this); // Nutrition adaptée au type de nutriment et de bactérie
}

Quantity SimpleBacterium::eatableQuantity(NutrimentB& nutriment)
{
   return nutriment.eatenBy(*this);    // Nutrition adaptée au type de nutriment et de bactérie
}

// Méthodes de mises à jour à chaque boucle_____________________________________________________________________________________________
void SimpleBacterium::drawOn(sf::RenderTarget& target) const
{
    // Dessin de la bacterie grâce à drawOn de Bacterium
    Bacterium::drawOn(target);

    // Création d'un ensemble de points
    auto set_of_points = sf::VertexArray(sf::LinesStrip);

    // ajout de points à l'ensemble:
    set_of_points.append({{0,0}, color_.get()}); //Ajouter le point (0,0)
    float x(0.), y(0.);
    for (int i(1); i<=30; ++i) {
        x = static_cast<float>(-i * getRadius() / 10.0);
        y = static_cast<float>(getRadius() * sin(t_) * sin(2*i/10.0));
        set_of_points.append({{x,y}, color_.get()}); // Ajouter le point (x,y)
    }

    // Déclaration d'une matrice de transformation
    auto transform = sf::Transform();

    // Ensemble d'opérations faites sur transform
    transform.translate(getPosition());
    transform.rotate(angleDirectionRad/DEG_TO_RAD);

    // Dessin de l'ensemble de points après transformation selon transform
    target.draw(set_of_points, transform);
}

// Méthodes d'extension________________________________________________________________________________________
bool SimpleBacterium::isVulnerable() const
{
    return true;
}
