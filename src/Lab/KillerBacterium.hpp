#pragma once
#include "SimpleBacterium.hpp"
#include <array>

class Virus;
class KillerBacterium : public SimpleBacterium
{
public:
    // constructeurs
    KillerBacterium(Vec2d center);
    KillerBacterium(Quantity energy, Vec2d center,Vec2d direction, double radius, MutableColor color);

    //Constructeur de copie
    KillerBacterium(KillerBacterium const&);

    // destructeur
    virtual ~KillerBacterium();

    // Méthodes héritées
    virtual void move(sf::Time dt) override;                           // déplacement
    virtual void drawOn(sf::RenderTarget& target) const override;      // dessin
    virtual void update(sf::Time dt) override;                         // mise à jour

    virtual Bacterium * clone() const override;                        // Clonage du KillerBacterium
    virtual j::Value const& getConfig() const override;                // Permet d'accéder aux valeurs de configuration de KillerBacterium
    virtual Quantity eatableQuantity(NutrimentA& nutriment) override;  // Fait appel à la méthode eatenBy de nutrimentA sur le bon type de bactérie
    virtual Quantity eatableQuantity(NutrimentB& nutriment) override;  // Fait appel à la méthode eatenBy de nutrimentB sur le bon type de bactérie
    virtual bool isVulnerable() const override;                        // Détermine si le type de bactérie est vulnérable aux attaques par les KillerBacterium

    // Méthodes spécifiques
    void setTarget();                             // Met à jour la cible d'une KillerBacterium
    void attack (Bacterium*);                     // Attaque une bactérie
    bool isVictim(Bacterium*) const;              // Détermine si la bactérie cible est vulnérable à son attaque (double dispatch)
    bool stillHasVirus() const;                   // Détermine si la bactérie est toujours porteuse dun virus
    void reset();                                 // Pour la destruction d'une KillerBacterium


    // Compteur de bacteries
    static int nb_killer;

private:
    Bacterium* target;     // Cible éventuelle de la KillerBacterium
    Virus* virus;          // Virus que porte la KillerBacterium
};
