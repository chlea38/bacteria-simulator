#include "NutrimentB.hpp"
#include "Application.hpp"
#include "Bacterium.hpp"
#include "SimpleBacterium.hpp"
#include "TwitchingBacterium.hpp"
#include "SwarmBacterium.hpp"
#include "KillerBacterium.hpp"

//Constructeur_________________________________________________________________________________________________
NutrimentB::NutrimentB(Quantity quantity, Vec2d center)
    : Nutriment(quantity, center)
{}

//Destructeur--------------------------------------------------------------------------------------------------
NutrimentB::~NutrimentB(){}

// Méthode spécifiques à la nutrition des bactéries____________________________________________________________
Quantity NutrimentB:: eatenBy(Bacterium& bact)
{
    return bact.eatableQuantity(*this);
}

Quantity NutrimentB::eatenBy(SimpleBacterium& bact)
{
     double factor (getConfig()["resistance factor"].toDouble());
     return takeQuantity(bact.getMaxEatableQuantity() / factor);
}

Quantity NutrimentB::eatenBy(TwitchingBacterium& bact)
{
     double factor1 (getConfig()["nutritive factor"].toDouble());
     Quantity qte(std::min(quantity_, bact.getMaxEatableQuantity()));
     return takeQuantity(qte*factor1);
}

Quantity NutrimentB::eatenBy(SwarmBacterium& bact)
{
    double factor2 (getConfig()["poison factor"].toDouble());
    Quantity qte(std::min(quantity_, bact.getMaxEatableQuantity()));
    return -takeQuantity(qte*factor2);
}

Quantity NutrimentB::eatenBy(KillerBacterium& bact)
{
    return takeQuantity(bact.getMaxEatableQuantity());
}

//Méthode pour faciliter accès aux données_____________________________________________________________________
j::Value const& NutrimentB:: getConfig() const
{
    return (getAppConfig()["nutriments"]["B"]);
}
