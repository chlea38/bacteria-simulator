#include "Lab.hpp"
#include "Application.hpp"
#include "SimpleBacterium.hpp"
#include "TwitchingBacterium.hpp"
#include "SwarmBacterium.hpp"
#include "../Utility/MutableNumber.hpp"

// Constructeur____________________________________________________________________________________________________
Lab::Lab()
    :dish_(getApp().getCentre(), 0.95*((getApp().getLabSize().x)/2))
{
    // On récupère le centre de la fenêtre de Lab et on set
    // le centre de l'assiette de pétri avec cette valeur.
    // On récupère aussi la largeur de la fenètre :  le rayon
    // de l'assiette est 95% de la moitié de la largeur
}
//destructeur__________________________________________________________________________________________________
Lab::~Lab(){}


//Getter__________________________________________________________________________________________________________
double Lab::getTemperature() const
{
    return dish_.getTemperature();
}

double Lab:: getGradientExponent() const
{
    return dish_.getGradientExponent();
}

double Lab::getPositionScore (const Vec2d& position) const
{
   return dish_.getPositionScore(position);
}

Swarm* Lab::getSwarmWithId(const std::string& id) const
{
    return dish_.getSwarmWithId(id);
}

int Lab::getNutrimentNumber() const
{
   return dish_.getNutrimentNumber();
}


// Méthodes de mise à jour à chaque boucle_________________________________________________________________________
void Lab::drawOn(sf::RenderTarget& targetWindow) const
{
    dish_.drawOn(targetWindow);
}


void Lab::update(sf::Time dt)
{
    dish_.update(dt);
    generator_.update(dt);
}

std::unordered_map<std::string, double> Lab::fetchData(const std::string & titre) const
{
    std::unordered_map<std::string, double> newdata;

    if (titre == s::GENERAL) {
        newdata[s::SIMPLE_BACTERIA] = SimpleBacterium::nb_simple;
        newdata[s::TWITCHING_BACTERIA] = TwitchingBacterium::nb_twitch;
        newdata[s::SWARM_BACTERIA] = SwarmBacterium::nb_swarm;
        newdata[s::NUTRIMENT_SOURCES] = getNutrimentNumber();
        newdata[s::DISH_TEMPERATURE] = getTemperature();

        return newdata;

    } else if (titre == s::SIMPLE_BACTERIA){
        newdata[s::BETTER] = getPropertySum("tumble better")/SimpleBacterium::nb_simple;
        newdata[s::WORSE] = getPropertySum("tumble worse")/SimpleBacterium::nb_simple;
        return newdata;

    } else if (titre == s::TWITCHING_BACTERIA) {
        newdata[s::TENTACLE_LENGTH] = getPropertySum("tentacle length")/TwitchingBacterium::nb_twitch;
        newdata[s::TENTACLE_SPEED] = getPropertySum("tentacle speed")/TwitchingBacterium::nb_twitch;
        return newdata;

    } else if (titre == s::NUTRIMENT_QUANTITY) {
        newdata[s::NUTRIMENT_QUANTITY] = getNutrimentNumber();
        return newdata;

    } else if (titre == s::BACTERIA) {
        newdata[s::SPEED] = getPropertySum("speed")/SimpleBacterium::nb_simple;
        return newdata;
    }
}


double Lab::getPropertySum(std::string key) const
{
    double sum(0);
    for (auto& bact : dish_.getBacterium()) {
        sum += bact->getProperty(key).get();
    }
    return sum;
}


//Méthodes appelées par le controle au clavier_____________________________________________________________________
void Lab:: reset()
{
    //Remise à zéro des conditions
    resetControls();
    //Destruction de tous les éléments dans la boite de pétri
    dish_.reset();
    generator_.reset();
}


void Lab::resetControls()
{
    auto const& temp (getAppConfig()["petri dish"]["temperature"]["default"].toDouble());
    dish_.setTemperature(temp);
    dish_.setGradientExponent((getAppConfig()["petri dish"]["gradient"]["exponent"]["max"].toDouble()+getAppConfig()["petri dish"]["gradient"]["exponent"]["min"].toDouble())/2);
}


bool Lab::addNutriment(Nutriment* n)
{
    return dish_.PetriDish::addNutriment(n);
}

bool Lab::addBacterium(Bacterium* b)
{
    return dish_.PetriDish::addBacterium(b);
}

void Lab::addSwarm(Swarm* swarm){
    dish_.PetriDish::addSwarm(swarm);
}

bool Lab:: addClone(Bacterium* c)
{
    return dish_.PetriDish::addClone(c);
}

void Lab::increaseTemperature()
{
    auto const& temp (getAppConfig()["petri dish"]["temperature"]["delta"].toDouble());
    dish_.setTemperature(dish_.getTemperature()+temp);
}


void Lab::decreaseTemperature()
{
    auto const& temp (getAppConfig()["petri dish"]["temperature"]["delta"].toDouble());
    dish_.setTemperature(dish_.getTemperature()-temp);
}

void Lab::increaseGradientExponent()
{
    auto const& grad (getAppConfig()["petri dish"]["gradient"]["exponent"]["delta"].toDouble());
    dish_.setGradientExponent(dish_.getGradientExponent()+grad);
}

void Lab::decreaseGradientExponent()
{
    auto const& grad (getAppConfig()["petri dish"]["gradient"]["exponent"]["delta"].toDouble());
    dish_.setGradientExponent(dish_.getGradientExponent()-grad);
}

//Méthode de test__________________________________________________________________________________________________
bool Lab:: contains(const CircularBody& c) const
{
    return dish_.CircularBody::contains(c);
}

bool Lab:: doesCollideWithDish(CircularBody const& body) const
{
    return (dish_.CircularBody::isColliding(body) and !dish_.CircularBody::contains(body));
}

Nutriment* Lab::getNutrimentColliding (const CircularBody& body ) const
{
    return dish_.getNutrimentColliding(body);
}

// Méthodes d'extention________________________________________________________________________________________
void Lab::addBacteriumToTab(Bacterium* bact)
{
    dish_.addBacteriumToTab(bact);
}

Case& Lab::getCase(int const& x, int const& y)
{
   return dish_.getCase(x,y);
}

void Lab::takeOutOfCase(Case& c,Bacterium* bact)
{
    dish_.takeOutOfCase(c, bact);
}
