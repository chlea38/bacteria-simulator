#pragma once
#include "PetriDish.hpp"
#include <SFML/Graphics.hpp>
#include"../Interface/Drawable.hpp"
#include"../Interface/Updatable.hpp"
#include "NutrimentGenerator.hpp"

class Lab : public Drawable, public Updatable
{
public:
    // Constructeur
    Lab();

    //Destructeur
    ~Lab();

    //Getters
    double getTemperature() const;                                // retourne la température du lab
    double getGradientExponent() const;                           // retourne le gradient de l'environnement
    double getPositionScore (const Vec2d&) const;                 // retourne le score à une position donnée
    Swarm* getSwarmWithId(const std::string& id) const;           // retourne le swarm correspondant à l'identifiant id
    int getNutrimentNumber() const;                               // retourne le nombre de nutriments dans l'assiette de pétri

    //Méthodes de mise à jour à chaque boucles
    virtual void drawOn (sf::RenderTarget& targetWindow) const override;            // dessin du lab
    virtual void update(sf::Time dt) override;                                      // mise à jour du lab
    std::unordered_map<std::string, double> fetchData(const std::string &) const;   // retourne les nouvelles données (après update) correspondantes aux graphes
    double getPropertySum(std::string key) const;                                   // Fait la somme des valeurs des propriétés mutables, utilisée dans fetchData


    //Méthodes appelées par le controle au clavier
    void reset();                       //R :remet tout à zéro (paramètres et contenu de la PetriDish)
    void resetControls();               //C : remet les paramètres de simulation à leur valeur de base
    bool addNutriment(Nutriment*);      //N : pas utilisé depuis NutrimentGenerator
    bool addBacterium(Bacterium*);      //S : SimpleBacterium, T : TwitchingBacterium, Num1 ou Num2 : SwarmBacterium
    void increaseTemperature();         //PgUp
    void decreaseTemperature();         //PgDn
    void increaseGradientExponent();    //PgUp
    void decreaseGradientExponent();    //PgDn

    // Autres méthodes d'ajout
    bool addClone(Bacterium*);          // ajoute une bactérie à l'assiette de pétri
    void addSwarm(Swarm* swarm);        // ajoute un swarm à l'assiette de pétri

    //Méthode de test
    bool contains(const CircularBody&) const;                            // Verifie si un circularbody est contenu dans un autre
    bool doesCollideWithDish(const CircularBody& body) const;            // vérifie si un circular body entre en collision avec l'assiette de pétri
    Nutriment* getNutrimentColliding (const CircularBody& body) const;   // retourne un nutriment si un circular body entre en collision avec

    //Méthodes d'extensions
    void addBacteriumToTab(Bacterium* bact);      // Ajoute une bactérie à la case appropriée
    Case& getCase(int const& x, int const& y);    // retourne la case ayant pour coordonnées (x,y)
    void takeOutOfCase(Case& c, Bacterium* bact); // supprime la bactérie de la case c

private:
    PetriDish dish_;                   // assiette de pétri
    NutrimentGenerator generator_;     // générateur de nutriment
};

