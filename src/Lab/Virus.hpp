#pragma once
#include "CircularBody.hpp"
#include "../Interface/Drawable.hpp"
#include "../Interface/Updatable.hpp"
#include "../Utility/Types.hpp"

class Bacterium;
class KillerBacterium;
class Virus : public CircularBody, public Drawable, public Updatable
{
public:

    // Constructeur
    Virus(Vec2d centre, KillerBacterium* carrier);

    // Destructeur
    virtual ~Virus();

    // Méthodes héritées
    virtual void drawOn(sf::RenderTarget &target) const override;  // dessin
    virtual void update(sf::Time dt) override;                     // mise à jour

    // Autres méthodes
    j::Value const& getConfig() const;        // Accès aux valeurs de configuration de virus
    void setTarget(Bacterium* bact);          // Met à jour bact comme cible du virus
    void move(sf::Time dt);                   // déplacement
    bool isDead() const;                      // Détermine si le virus est mort
    void consumeEnergy (Quantity const& qt);  // consomme de l'energie lors de l'attaque

private:
    Quantity energy;               // niveau d'energie
    Bacterium* target;             // bacterie qui est la cible du virus
    KillerBacterium* carrier;      // KillerBacterium qui porte le virus
    sf::Time compteur;             // compteur de temps
};


