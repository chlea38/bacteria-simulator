#include "NutrimentGenerator.hpp"
#include <SFML/Graphics.hpp>
#include "Application.hpp"
#include "../Random/Random.hpp"
#include "NutrimentA.hpp"
#include "NutrimentB.hpp"



//Constructeur_________________________________________________________________________________________________
NutrimentGenerator::NutrimentGenerator():timer(sf::Time::Zero)
{}


//Destructeur__________________________________________________________________________________________________
NutrimentGenerator::~NutrimentGenerator()
{}


//Méthodes de mise à jour à chaque boucle______________________________________________________________________
void NutrimentGenerator::update(sf::Time dt)
{
    //Incrémentation du compteur
    timer+=dt;

    //Récupération de la valeur seuil
    auto const& maxTime (sf::seconds(getAppConfig()["generator"]["nutriment"]["delay"].toDouble()));

    //Actions si dépassement de la valeur seuil
    if (timer>maxTime)
    {
        // remise à zero du compteur
        reset();

        //Choix aléatoire d'un nutriment de type A ou B
        bool typeA=bernoulli(getAppConfig()["generator"]["nutriment"]["prob"].toDouble());

        // Récupération de la taille de l'environnement
        Vec2d size (getApp().getLabSize());
        // Génération d'un placement pour le nutriment de coordonnées x,y
        double x (normal(size[0]/2, (size[0]/4)*(size[0]/4)));
        double y (normal(size[1]/2, (size[1]/4)*(size[1]/4)));
        Vec2d position (x,y);

        if(typeA)  //Création d'un nutriment de type A
        {
            // Génération aléatoire d'une quantité de nutriment
            Quantity q (uniform((getShortConfig().nutrimentA_min_qty),getShortConfig().nutrimentA_max_qty));

            //Ajout du nouveau nutriment
            getAppEnv().addNutriment(new NutrimentA (q,position));
        }
        else    // Création d'un nutriment de type B
        {
            // Génération d'une quantité de nutriment associée
            Quantity q (uniform((getShortConfig().nutrimentB_min_qty),getShortConfig().nutrimentB_max_qty));

            //Ajout du nouveau nutriment
            getAppEnv().addNutriment(new NutrimentB (q,position));
        }
    }
}


//Autres méthodes______________________________________________________________________________________________
void NutrimentGenerator:: reset()
{
    timer =sf::Time::Zero;
}
