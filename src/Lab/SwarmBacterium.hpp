#pragma once
#include "Bacterium.hpp"
#include "../Utility/DiffEqSolver.hpp"

class Swarm;
class SwarmBacterium: public Bacterium, public DiffEqFunction
{   
public:
    // Constructeur
    SwarmBacterium(Vec2d pos, Swarm* group);

    //Constructeur de copie
    SwarmBacterium(SwarmBacterium const&);

    //Destructeur
    virtual ~SwarmBacterium() override;

    // getter
    virtual MutableNumber getProperty(const std::string& key) const override;  // retrouve la propriété mutable associée à key

    // Méthodes de mise à jour à chaque boucle
    virtual void drawOn(sf::RenderTarget& target) const override; // dessin

    // Méthodes d'évolution des bactéries
    virtual Bacterium* clone()const override;                     // clonage
    virtual void move(sf::Time dt) override;                      // déplacement

    // Méthodes d'accès aux données
    virtual Vec2d f(Vec2d position, Vec2d speed) const override;  // Calcul de la force
    virtual j::Value const& getConfig() const override;           // renvoit à la partie SwarmBacterium du fichier de configuration
    Vec2d getSpeedVector() const;                                 // Calcul du vecteur vitesse

    // Méthodes spécifiques à la nutrition des bactéries
    virtual Quantity eatableQuantity(NutrimentA& nutriment) override ;       // Fait appel à la méthode eatenBy(SwarmBacterium& bact) de nutrimentA
    virtual Quantity eatableQuantity(NutrimentB& nutriment) override;        // Fait appel à la méthode eatenBy(SwarmBacterium& bact) de nutrimentB

    // Compteur de bacteries
    static int nb_swarm;

    // Méthodes d'extension
    virtual bool isVulnerable() const override;     // détermine si la bactérie est vulnérable à une attaque par une killer

private:
    Swarm* swarm;
};



