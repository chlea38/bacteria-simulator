#pragma once
#include "../Interface/Updatable.hpp"
#include <SFML/Graphics.hpp>

class NutrimentGenerator : public Updatable
{
public:
    //Constructeur
    NutrimentGenerator();

    //Destructeur
    ~NutrimentGenerator();

    //Méthodes de mise à jour à chaque boucle
     void update(sf::Time dt) override;

    //Autres méthodes
     void reset();       // remise à zero du compteur

private:
    sf::Time timer;     // compteur

};


