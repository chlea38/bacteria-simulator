#include <Stats/Stats.hpp>
#include <iostream>
#include <Application.hpp>

// Constructeur_______________________________________________________________
Stats::Stats()
    : identifiant_actif(0), compteur(sf::Time::Zero)
{}

// Destructeur_________________________________________________________________
Stats::~Stats()
{
    for (auto& graphe : graph_) {
        graphe.graph->reset();
    }
    graph_.clear();
}


// Méthodes de mises à jour à chaque boucle_______________________________________________
void Stats::drawOn(sf::RenderTarget& target) const
{
    graph_[identifiant_actif].graph->drawOn(target);
}

void Stats::update(sf::Time dt)
{
    //Incrémentation du compteur
    compteur+=dt;

    //Récupération de l'intervalle de temps
    auto const& maxTemps (sf::seconds(getAppConfig()["stats"]["refresh rate"].toDouble()));

    //Actions si dépassement de la valeur seuil
    if (compteur>maxTemps) {
        // On met a jour tous les graphes
        for (auto& graphe : graph_) {
            // calculer les nouvelles données
            std::unordered_map<std::string, double> new_data = getAppEnv().fetchData(graphe.libelle); // PAS SURE DU TITRE

            // insertion des nouvelles données dans le graphe
            graphe.graph->updateData(compteur, new_data);
        }

        // Remise à 0 du compteur
        compteur = sf::Time::Zero;
    }
}

// Méthodes_______________________________________________________________
std::string Stats::getCurrentTitle()
{
    // Retourne le libellé du graphe courant
    return graph_[identifiant_actif].libelle;
}


void Stats::next()
{
    if (identifiant_actif == graph_.size() - 1) {
        identifiant_actif = 0;
    } else if (identifiant_actif < graph_.size() - 1) {
        // On incrémente l'identifiant actif
        ++identifiant_actif;
    }
}

void Stats::previous()
{
    if (identifiant_actif == 0) {
        identifiant_actif = graph_.size() -1;
    } else if (identifiant_actif > 0 ) {
        // On décrémente l'identifiant actif
        --identifiant_actif;
    }
}

void Stats::reset()
{
    for (auto& graphique : graph_) {
        graphique.graph->reset();
    }
}


void Stats::addGraph(int id, std::string titre, std::vector<std::string> ensemble_titre,
              double min, double max, Vec2d size)
{   
    // Ajout d'un graphe créé au moyen du constructeur de graph, associé à un titre
    graph_.push_back({std::unique_ptr<Graph>(new Graph(ensemble_titre, size, min, max)), titre});

    // mise à jour de l'identifiant actif
    setActive(id);

}


void Stats::setActive(int id)
{
    identifiant_actif = id;
}
