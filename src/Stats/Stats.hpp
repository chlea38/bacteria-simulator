#pragma once
#include "Graph.hpp"
#include <vector>
#include "../Interface/Drawable.hpp"
#include "../Interface/Updatable.hpp"

struct Graphique {
    std::unique_ptr<Graph> graph;
    std::string libelle;
};

class Stats : public Updatable, public Drawable
{
public:
    // Constructeur
    Stats();

    // Destructeur
    ~Stats();

    // Méthodes de mise à jour a chaque boucle
    virtual void drawOn(sf::RenderTarget& target) const override final;
    virtual void update(sf::Time dt) override final;

    // Méthodes
    std::string getCurrentTitle();
    void next();
    void previous();
    void reset();
    void addGraph(int id, std::string titre, std::vector<std::string> ensemble_titre,
                  double min, double max, Vec2d size);
    void setActive(int id);


private:
    std::vector<Graphique> graph_;
    int identifiant_actif;
    sf::Time compteur;
};

